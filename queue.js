let collection = [];

// Write the queue functions below.
function print() {
  return (collection = []);
}
print();

function enqueue(enqueued) {
  collection[collection.length] = enqueued;
  return collection;
}

enqueue("John");
enqueue("Jane");
function dequeue() {
  collection.splice(0, 1);
  return collection;
}
dequeue();

enqueue("Bob");
enqueue("Cherry");

function front() {
  return collection[0];
}
function isEmpty() {
  return collection.length == 0;
}
function size() {
  return collection.length;
}
console.log(collection);
front();
isEmpty();
size();
// Export create queue functions below.
module.exports = {
  print,
  enqueue,
  dequeue,
  front,
  isEmpty,
  size,
};
